# Library Project

This project was developed by Lilian Delouvy for ARI course, in second year of Master's degree.

## How to use

- Clone the project (https://gitlab.com/lilian-delouvy/ari_library.git)
- While running it, you can use different email addresses to test the different user types:
    * For READERS: please use "normal.user@gmail.com" with password "password"
    * For ADMINS: please use "admin@gmail.com" with password "admin"

## Features

This project includes the following features:

The reader can access to the books list and reserve one, that he will be able to see in a dedicated page. He can also change the language of the website.

Obviously, the reader is able to sign up and log in.

The admin can do everything described above too, but he also can add authors, books or even readers. He can access to the list of readers and update them or delete them.
