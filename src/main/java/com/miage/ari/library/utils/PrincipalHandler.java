package com.miage.ari.library.utils;

import com.miage.ari.library.bo.Reader;
import org.springframework.security.core.context.SecurityContextHolder;

public class PrincipalHandler {

    public static Reader getPrincipalReader(){
        return (Reader) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

}
