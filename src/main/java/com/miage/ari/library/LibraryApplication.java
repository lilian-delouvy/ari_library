package com.miage.ari.library;

import com.miage.ari.library.bo.*;
import com.miage.ari.library.repository.AuthorRepository;
import com.miage.ari.library.repository.BookRepository;
import com.miage.ari.library.repository.LibraryRepository;
import com.miage.ari.library.repository.ReaderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;

@SpringBootApplication
public class LibraryApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibraryApplication.class, args);
	}

	@Bean
	@Autowired
	public CommandLineRunner demo(ReaderRepository readerRepository, LibraryRepository libraryRepository, AuthorRepository authorRepository, BookRepository bookRepository) {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		return (args) -> {
			var author1 = new Author(1, "Andy Weir");
			var author2 = new Author(2, "Joss Whedon");
			authorRepository.save(author1);
			authorRepository.save(author2);

			var book1 = new Novel(1, "Seul sur Mars", new ArrayList<>());
			var book2 = new Comic(2, "X-Men", new ArrayList<>(), "X-Men: Wolverine");
			book1.addAuthor(author1);
			book2.addAuthor(author2);
			author1.addBook(book1);
			author2.addBook(book2);
			bookRepository.save(book1);
			bookRepository.save(book2);
			authorRepository.save(author1);
			authorRepository.save(author2);

			var reader = new Reader(1, "John Doe", "normal.user@gmail.com");
			reader.setPassword(bCryptPasswordEncoder.encode("password"));
			var readerAdmin = new Reader(2, "Lilian Delouvy", "admin@gmail.com");
			readerAdmin.setPassword(bCryptPasswordEncoder.encode("admin"));
			readerAdmin.setRole("ADMIN");
			readerRepository.save(reader);
			readerRepository.save(readerAdmin);

			var library = new Library(1, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
			library.addReader(reader);
			library.addAuthor(author1);
			library.addAuthor(author2);
			library.addBook(book1);
			library.addBook(book2);
			libraryRepository.save(library);
		};
	}

}
