package com.miage.ari.library.controller;

import com.miage.ari.library.service.LibraryService;
import com.miage.ari.library.utils.PrincipalHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LibraryController {

    public LibraryService libraryService;

    @GetMapping("")
    public String index() {
        return "index";
    }

    @GetMapping("/books")
    public ModelAndView booksPage() {
        ModelAndView model = new ModelAndView();
        var reader = PrincipalHandler.getPrincipalReader();
        var book = reader.getCurrentBook();
        model.addObject("currentBook", book);
        model.addObject("books_list", libraryService.getAllBooks());
        model.setViewName("books");
        return model;
    }

    @GetMapping("/book")
    public ModelAndView currentBookPage(){
        ModelAndView model = new ModelAndView();
        var reader = PrincipalHandler.getPrincipalReader();
        var book = reader.getCurrentBook();
        model.addObject("book", book);
        model.setViewName("book_details");
        return model;
    }

    @PostMapping("/book")
    @ResponseBody
    public String removeBook(){
        var reader = PrincipalHandler.getPrincipalReader();
        var book = reader.getCurrentBook();
        reader.setCurrentBook(null);
        book.setCurrentReader(null);
        libraryService.createReader(reader);
        libraryService.createBook(book);
        return "success";
    }

    @PostMapping("/takeBook/{bookId}")
    @ResponseBody
    public String takeBook(@PathVariable int bookId){
        var reader = PrincipalHandler.getPrincipalReader();
        var book = libraryService.getBook(bookId);
        //if the book hasn't already been taken by another reader
        if(book.getCurrentReader() == null){
            //if the reader already had a book, this book becomes available
            var returnedBook = reader.getCurrentBook();
            if(returnedBook != null){
                returnedBook.setCurrentReader(null);
                libraryService.createBook(returnedBook);
            }
            reader.setCurrentBook(book);
            book.setCurrentReader(reader);
            libraryService.createReader(reader);
            libraryService.createBook(book);
        }
        return "success";
    }

    @Autowired
    public void setLibraryService(LibraryService libraryService) {
        this.libraryService = libraryService;
    }

}
