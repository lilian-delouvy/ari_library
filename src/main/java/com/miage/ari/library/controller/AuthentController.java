package com.miage.ari.library.controller;

import com.miage.ari.library.bo.Reader;
import com.miage.ari.library.service.ReaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class AuthentController {

    public ReaderService readerService;

    @GetMapping("/login")
    public String loginPage(){
        return "login";
    }

    @GetMapping("/signup")
    public ModelAndView signupPage(ModelAndView model, @ModelAttribute("error") final String error){
        if(error != null){
            model.addObject("error", error);
        }
        model.setViewName("signup");
        return model;
    }

    @PostMapping("/signup")
    public ModelAndView signup(@ModelAttribute("reader") Reader reader, BindingResult result, ModelAndView model, RedirectAttributes redirectAttributes){
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        if(readerService.findByEmail(reader.email) == null){
            reader.setPassword(bCryptPasswordEncoder.encode(reader.password));
            readerService.addReader(reader);
            model.setViewName("redirect:/login");
        }
        else{
            redirectAttributes.addFlashAttribute("error", "L'adresse email est déjà utilisée !");
            model.setViewName("redirect:/signup");
        }
        return model;
    }

    @Autowired
    void setReaderService(ReaderService readerService){
        this.readerService = readerService;
    }

}
