package com.miage.ari.library.controller;

import com.miage.ari.library.bo.*;
import com.miage.ari.library.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

@Controller
@RequestMapping("/admin")
public class AdminController {

    public LibraryService libraryService;

    @GetMapping("/readers")
    public ModelAndView readers(){
        ModelAndView model = new ModelAndView();
        model.addObject("readers_list",libraryService.getAllReaders());
        model.setViewName("readers");
        return model;
    }

    @GetMapping("/addReader")
    public String addReaderPage(Model model){
        model.addAttribute("reader", new Reader());
        return "admin/addReader";
    }

    @PostMapping("/addReader")
    public void createReader(@ModelAttribute Reader reader){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if(reader.getName() != null && !reader.getName().isEmpty()){
            if(reader.getName().matches("^[a-zA-Z\\s]*$")){
                reader.setPassword(encoder.encode(reader.password));
                libraryService.createReader(reader);
            }
        }
    }

    @GetMapping("/addAuthor")
    public String addAuthorPage(Model model){
        model.addAttribute("author", new Author());
        return "admin/addAuthor";
    }

    @PostMapping("/addAuthor")
    public void createAuthor(@ModelAttribute Author author){
        if(author.getName() != null && !author.getName().isEmpty()){
            libraryService.createAuthor(author);
        }
    }

    @GetMapping("/addBook")
    public String addBookPage(Model model){
        model.addAttribute("formBook", new FormBook("", "", new ArrayList<>()));
        model.addAttribute("authors", libraryService.getAllAuthors());
        return "admin/addBook";
    }

    //We allow the creation of a book without author, as some books don't have a recognized author
    @PostMapping("/addBook")
    public String addBook(@ModelAttribute FormBook formBook, Model model) {
        Book book;
        if (formBook.getSeries().isEmpty()) {
            book = new Novel(0, formBook.getTitle(), formBook.getAuthors());
        } else {
            book = new Comic(0, formBook.getTitle(), formBook.getAuthors(), formBook.getSeries());
        }
        libraryService.createBook(book);
        for (Author author : formBook.getAuthors()) {
            author.addBook(book);
            libraryService.createAuthor(author);
        }
        return addBookPage(model);
    }

    @GetMapping("/updateReader/{id}")
    public String updateReaderPage(Model model, @PathVariable int id){
        model.addAttribute("reader", libraryService.getReader(id));
        return "admin/updateReader";
    }

    /*
        No need to make another method, we can use the "createReader" also used in post mapping above
        We still use a different route in case of code evolutions
     */
    @PostMapping("/updateReader")
    public void updateReader(@ModelAttribute Reader reader){
        if(reader.getName() != null){
            if(reader.getName().matches("^[a-zA-Z\\s]*$")){
                libraryService.createReader(reader);
            }
        }
    }

    @DeleteMapping("/deleteReader/{id}")
    public String deleteReader(@PathVariable int id){
        var reader = libraryService.getReader(id);
        var currentBook = reader.getCurrentBook();
        if(currentBook != null){
            currentBook.setCurrentReader(null);
        }
        libraryService.deleteReader(id);
        return "index";
    }

    @Autowired
    void setLibraryService(LibraryService libraryService){
        this.libraryService = libraryService;
    }
}
