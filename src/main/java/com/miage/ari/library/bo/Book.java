package com.miage.ari.library.bo;

import javax.persistence.*;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    public int id;

    public String title;

    @ManyToMany
    public List<Author> authors;

    @OneToOne
    public Reader currentReader;

    public Book(int id, String title, List<Author> authors) {
        this.id = id;
        this.title = title;
        this.authors = authors;
        this.currentReader = null;
    }

    public Book(){

    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void addAuthor(Author newAuthor){
        authors.add(newAuthor);
    }

    public Reader getCurrentReader() {
        return currentReader;
    }

    public void setCurrentReader(Reader currentReader) {
        this.currentReader = currentReader;
    }
}
