package com.miage.ari.library.bo;

import javax.persistence.Entity;
import java.util.List;

@Entity
public class Novel extends Book{

    public Novel(int id, String title, List<Author> authors) {
        super(id, title, authors);
    }

    public Novel(){

    }
}
