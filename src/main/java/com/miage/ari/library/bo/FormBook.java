package com.miage.ari.library.bo;

import java.util.List;

//This class is required for the "addBook" page of the AdminController
public class FormBook {

    public String title;

    public String series;

    public List<Author> authors;

    public FormBook(String title, String series, List<Author> authors) {
        this.title = title;
        this.series = series;
        this.authors = authors;
    }

    public FormBook(){

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }
}
