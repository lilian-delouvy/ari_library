package com.miage.ari.library.bo;

import javax.persistence.*;
import java.util.List;

@Entity
public class Library {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @OneToMany
    public List<Reader> readers;

    @OneToMany
    public List<Book> books;

    @OneToMany
    public List<Author> authors;

    public Library(int id, List<Reader> readers, List<Book> books, List<Author> authors) {
        this.id = id;
        this.readers = readers;
        this.books = books;
        this.authors = authors;
    }

    public Library(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Reader> getReaders() {
        return readers;
    }

    public void setReaders(List<Reader> readers) {
        this.readers = readers;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public void addAuthor(Author author){
        authors.add(author);
    }

    public void addBook(Book book){
        books.add(book);
    }

    public void addReader(Reader reader){
        readers.add(reader);
    }

    public void removeReader(Reader reader){
        readers.remove(reader);
    }

    public void removeReaderById(int readerId){
        readers.removeIf(reader -> reader.id == readerId);
    }
}
