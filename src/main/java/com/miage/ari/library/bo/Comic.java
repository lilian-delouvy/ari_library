package com.miage.ari.library.bo;

import javax.persistence.*;
import java.util.List;

@Entity
public class Comic extends Book{

    public String series;

    public Comic(int id, String title, List<Author> authors, String series) {
        super(id, title, authors);
        this.series = series;
    }

    public Comic(){

    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }
}
