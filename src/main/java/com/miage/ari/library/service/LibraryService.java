package com.miage.ari.library.service;

import com.miage.ari.library.bo.Author;
import com.miage.ari.library.bo.Book;
import com.miage.ari.library.bo.Reader;

public interface LibraryService {
    void createReader(Reader reader);
    void deleteReader(int id);
    void createBook(Book book);
    void deleteBook(int id);
    void createAuthor(Author author);
    Iterable<Reader> getAllReaders();
    Iterable<Book> getAllBooks();
    Iterable<Author> getAllAuthors();
    Book getBook(int id);
    Reader getReader(int id);
}
