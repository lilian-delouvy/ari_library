package com.miage.ari.library.service;

import com.miage.ari.library.bo.Author;
import com.miage.ari.library.bo.Book;
import com.miage.ari.library.bo.Reader;
import com.miage.ari.library.repository.AuthorRepository;
import com.miage.ari.library.repository.BookRepository;
import com.miage.ari.library.repository.LibraryRepository;
import com.miage.ari.library.repository.ReaderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LibraryServiceImpl implements LibraryService{

    protected LibraryRepository libraryRepository;

    protected ReaderRepository readerRepository;

    protected BookRepository bookRepository;

    protected AuthorRepository authorRepository;

    @Autowired
    public void setLibraryRepository(LibraryRepository libraryRepository){
        this.libraryRepository = libraryRepository;
    }

    @Autowired
    public void setReaderRepository(ReaderRepository readerRepository) {
        this.readerRepository = readerRepository;
    }

    @Autowired
    public void setLibraryRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Autowired
    public void setAuthorRepository(AuthorRepository authorRepository){
        this.authorRepository = authorRepository;
    }

    @Override
    public void createReader(Reader reader) {
        readerRepository.save(reader);
    }

    @Override
    public void deleteReader(int id) {
        var library = libraryRepository.findById(1).orElse(null);
        if(library != null){
            library.removeReaderById(id);
            libraryRepository.save(library);
        }
        readerRepository.deleteById(id);
    }

    @Override
    public void createBook(Book book) {
        bookRepository.save(book);
    }

    @Override
    public void deleteBook(int id) {
        bookRepository.deleteById(id);
    }

    @Override
    public void createAuthor(Author author) {
        authorRepository.save(author);
    }

    @Override
    public Iterable<Reader> getAllReaders() {
        return readerRepository.findAll();
    }

    @Override
    public Iterable<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public Iterable<Author> getAllAuthors() {
        return authorRepository.findAll();
    }

    @Override
    public Book getBook(int id) {
        return bookRepository.findById(id).orElse(null);
    }

    @Override
    public Reader getReader(int id) {
        return readerRepository.findById(id).orElse(null);
    }
}
