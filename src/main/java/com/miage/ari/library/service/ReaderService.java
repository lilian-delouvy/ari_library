package com.miage.ari.library.service;

import com.miage.ari.library.bo.Reader;
import com.miage.ari.library.repository.ReaderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class ReaderService implements UserDetailsService {

    @Autowired
    private ReaderRepository readerRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Reader user = readerRepository.findByEmail(s);
        if(user == null){
            throw new UsernameNotFoundException(s);
        }
        return user;
    }

    public Reader findByEmail(String email){
        return readerRepository.findByEmail(email);
    }

    public void addReader(Reader reader){
        readerRepository.save(reader);
    }
}
