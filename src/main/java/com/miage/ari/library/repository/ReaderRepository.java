package com.miage.ari.library.repository;

import com.miage.ari.library.bo.Reader;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReaderRepository extends CrudRepository<Reader, Integer> {
    @Query("select r from Reader r where r.email = ?1")
    Reader findByEmail(String email);
}
